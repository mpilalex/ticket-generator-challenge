function NumberRandomizer(numbers){
    
    let copy = numbers.slice(0);
    let min = 0;
    let max = copy.length - 1;
    

    const removeIndex = index => {
        copy.splice(index, 1);
        max = copy.length - 1;
    }

    return {
        next(){
            if(!copy.length){
                return -1;
            }            
            const idx = copy.length == 1 ? 0 : Math.floor(Math.random() * (max - min + 1)) + min;
            const val = copy[idx];
            removeIndex(idx);
            return val;
        },
        getNumbers(){
            return copy;
        },
        hasNext(){
            return copy.length > 0;
        }            
    }

}

module.exports = NumberRandomizer;