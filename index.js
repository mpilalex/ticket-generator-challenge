const NumberRandomizer = require("./NumberRandomizer");
const _ = require("underscore");
const LoggerFactory = require("js-logger");
LoggerFactory.useDefaults();

function BingoTicket(index) {

    const logger = LoggerFactory.get(`Ticket: ${index}`);

    const lines = [
        [null, null, null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null, null, null]
    ];

    const resetLines = () => {
        lines.forEach(line => line.forEach((v, i) => line[i] = null));
    }



    const addToLine = (lineIndexRand, col, val) => {
        const lineIndex = lineIndexRand.next();
        if (lines[lineIndex][col] == null && lines[lineIndex].filter(v => v != null).length < 5) {
            lines[lineIndex][col] = val;
            return true;
        }
        if (lineIndexRand.hasNext()) {
            addToLine(lineIndexRand, col, val);
        } else {
            //logger.info(`Can not add val: ${val} in col: ${col}`);
            return false;
        }

    }

    const getAvailableLineIndexes = () => {
        const res = [];
        for (let i = 0; i < lines.length; i++) {
            if (lines[i].filter(v => v != null).length < 5) {
                res.push(i);
            }
        }
        return res;
    }

    const addToLines = (col, count, num) => {
        while (count > 0) {
            if (!addToLine(NumberRandomizer(getAvailableLineIndexes()), col, num)) {
                return false;
            };
            count--;
        }
        return true;
    }

    const sortFn = (arr, minIndex = 0) => {

        let min = arr[minIndex];
        let hasChanges = false;
        for (let i = minIndex; i < arr.length; i++) {
            const val = arr[i];
            if (val != null && val < min) {
                arr[i] = min;
                arr[minIndex] = val;
                minIndex = i;
                min = val;
                hasChanges = true;
            }
        }
        if (hasChanges) {                    
            sortFn(arr, minIndex);
        }
    }

    return {

        print() {
            logger.info("Lines");
            console.log(" --------------------------");
            lines.forEach(line => {
                const printNumbers = line.map(num => {
                    return num == null ? "  " : new String(num).length == 1 ? ` ${num}` : num;
                });
                console.log(`|${printNumbers.join("|")}|`);
            })
            console.log(" --------------------------");
            //logger.info(colsConfig, getNumbersSum(), this.getConfigCount());
        },

        getNumbersSum() {
            return getNumbersSum();
        },


        fillPlaces(config) {
            const copy = config.slice(0);
            copy.sort().reverse();
            const colIndexRand = NumberRandomizer([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
            let res = true;
            while (colIndexRand.hasNext()) {
                const col = colIndexRand.next()
                if (!addToLines(col, config[col], 1)) {
                    res = false;
                    break;
                }
            }
            if (!res) {
                resetLines();
                this.fillPlaces(config);
            }

        },

        fillValues(availableNumbers) {
            lines.forEach(line => line.forEach((v, i) => {
                if (v == 1) {
                    line[i] = availableNumbers[i].next();
                }
            }));
        },

        sortCols() {
            
            for (let col = 0; col < 9; col++) {
                const arr = lines.map(line => line[col]);
                sortFn(arr);
                arr.forEach((v, i) => {
                    lines[i][col] = v;
                })                
            }
        }

    }
}

function BingoStripe() {

    const stripeNumbers = [
        NumberRandomizer([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        NumberRandomizer([10, 11, 12, 13, 14, 15, 16, 17, 18, 19]),
        NumberRandomizer([20, 21, 22, 23, 24, 25, 26, 27, 28, 29]),
        NumberRandomizer([30, 31, 32, 33, 34, 35, 36, 37, 38, 39]),
        NumberRandomizer([40, 41, 42, 43, 44, 45, 46, 47, 48, 49]),
        NumberRandomizer([50, 51, 52, 53, 54, 55, 56, 57, 58, 59]),
        NumberRandomizer([60, 61, 62, 63, 64, 65, 66, 67, 68, 69]),
        NumberRandomizer([70, 71, 72, 73, 74, 75, 76, 77, 78, 79]),
        NumberRandomizer([80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90])
    ];

    const stripe = [];
    for (let i = 0; i < 6; i++) {
        stripe.push(Array(9).fill(0))
    }


    const logger = LoggerFactory.get("BingoStripe");


    const assignNumbersByTicket = [
        [[3, 2, 1, 1, 1, 1], [2, 2, 2, 1, 1, 1]],
        [[3, 3, 1, 1, 1, 1], [3, 2, 2, 1, 1, 1], [2, 2, 2, 2, 1, 1]],
        [[3, 3, 2, 1, 1, 1], [3, 2, 2, 2, 1, 1], [2, 2, 2, 2, 2, 1]],
    ];

    const fillStripColumn = (stripe, col, arr) => {
        //console.log("ARR", arr);
        arr.forEach((v, i) => { stripe[i][col] = Number(v) })
    }

    const isValid = (stripe, index) => {
        return !stripe.some((row, ri) => {
            //console.log(`SOME ROW - ${index}`, row, );
            let sum = row.reduce((total, v) => {
                return total + v
            }, 0);
            //console.log(`SOME ROW - ${index}`, sum, row);
            if ((sum + 8 - index) > 15) return true
            return false
        })
    }

    const removeStripColumn = (strip, col) => {
        //console.log("ARR", arr);
        strip.forEach(arr => arr[col] = 0)
        //arr.forEach((v, i) => { strip[i][col] = Number(v) })
    }

    const getArr = i => {
        let numbersByTickets = null;
        switch (i) {
            case 0:
                numbersByTickets = _.shuffle(assignNumbersByTicket[0][NumberRandomizer([0, 1]).next()]);
                break;
            case 8:
                numbersByTickets = _.shuffle(assignNumbersByTicket[2][NumberRandomizer([0, 1, 2]).next()])
                break;
            default:
                numbersByTickets = _.shuffle(assignNumbersByTicket[1][NumberRandomizer([0, 1, 2]).next()])
                break;
        }
        return numbersByTickets;
    }

    let linesCount = 0;

    while (linesCount < 9) {
        fillStripColumn(stripe, linesCount, getArr(linesCount));
        if (isValid(stripe, linesCount)) {
            linesCount++;
        } else {
            removeStripColumn(stripe, linesCount);
            if (linesCount > 0) {
                linesCount--
            }
            //linesCount--;
        }
    }

    const tickets = [];

    stripe.forEach((ticketConfig, index) => {
        const ticket = new BingoTicket(index);
        ticket.fillPlaces(ticketConfig)
        tickets.push(ticket);
    });

    tickets.forEach(ticket => {
        ticket.fillValues(stripeNumbers);
    })


    tickets.forEach(ticket => {
        ticket.sortCols();
        ticket.print();
    })

}


BingoStripe();